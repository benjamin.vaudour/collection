# collection

collection est une librairie Go pour manipuler des collections de variables

## Installation

La librairie peut être installée dans le répertoire $GOPATH via la commande suivante :

```
go get framagit.org/benjamin.vaudour/collection
```

ou bien être utilisée directement dans un projet (nécessite Go ≥ 1.12)  via un import :

```go
import (
	"framagit.org/benjamin.vaudour/collection"
)
```
