package collection

//Set est une collection de variables uniques
type Set map[interface{}]bool

//Add ajoute au set les variables données en paramètre
func (s Set) Add(args ...interface{}) Set {
	for _, e := range args {
		s[e] = true
	}
	return s
}

//Del supprime du set les variables données en paramètre
func (s Set) Del(args ...interface{}) Set {
	for _, e := range args {
		delete(s, e)
	}
	return s
}

//Contains vérifie que le set contient au moins une des variables données en paramètre
func (s Set) Contains(args ...interface{}) bool {
	for _, e := range args {
		if s[e] {
			return true
		}
	}
	return false
}

//ContainsAll vérifie que le set contient toutes les variables données en paramètre
func (s Set) ContainsAll(args ...interface{}) bool {
	for _, e := range args {
		if !s[e] {
			return false
		}
	}
	return true
}

//Slice retourne un slice avec toutes les entrées du set
func (s Set) Slice() []interface{} {
	var out []interface{}
	for e := range s {
		out = append(out, e)
	}
	return out
}

//Len retourne le nombre de variables contenues dans le set
func (s Set) Len() int {
	i := 0
	for _, v := range s {
		if v {
			i++
		}
	}
	return i
}

//NewSet retourne un set initialisé par les variables données en entrée
func NewSet(args ...interface{}) Set {
	set := make(Set)
	return set.Add(args...)
}
