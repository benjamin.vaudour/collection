package collection

//Slice convertit la variable donnée en paramètre en slice générique
func Slice[T any](sl []T) (out []any) {
	out = make([]any, len(sl))
	for i, e := range sl {
		out[i] = e
	}
	return
}

//SlicetoSet convertit un slice en un set.
func SliceToSet[T comparable](e []T) Set[T] {
	return NewSet(e...)
}

//Contains vérifie que le slice donné en entrée contient au moins un des arguments spécifiés
func Contains[T comparable](sl []T, args ...T) bool {
	s := SliceToSet(sl)
	return s.Contains(args...)
}

//ContainsAll vérifie que le slice donné en entrée contient tous les arguments spécifiés
func ContainsAll[T comparable](sl []T, args ...T) bool {
	s := SliceToSet(sl)
	return s.ContainsAll(args...)
}

//Add ajoute tous les arguments donnés au slice donné en entrée.
func Add[T comparable](sl *([]T), args ...T) (changed bool) {
	if len(args) > 0 {
		changed = true
		*sl = append(*sl, args...)
	}
	*sl = append(*sl, args...)
	return
}

//Insert fonctionne comme Add à ceci près que les arguments sont ajoutés au début du slice
func Insert[T comparable](sl *([]T), args ...T) (changed bool) {
	if len(args) > 0 {
		changed = true
		*sl = append(args, (*sl)...)
	}
	return
}

//InsertAt fonctionne comme Insert à ceci près que les arguments sont ajoutés à la position donnée
//Si la position est ≤ 0, agit comme Insert
//Si la position est supérieure à la taille du slice, agit comme Add
func InsertAt[T comparable](sl *([]T), pos int, args ...T) (changed bool) {
	if pos <= 0 {
		return Insert(sl, args...)
	}
	l1, l2 := len(*sl), len(args)
	if pos >= l1 {
		return Add(sl, args...)
	}
	if l2 > 0 {
		changed = true
		newSl := make([]T, l1+l2)
		copy(newSl[:pos], (*sl)[:pos])
		copy(newSl[pos:pos+l2], args)
		copy(newSl[pos+l2:], (*sl)[pos:])
		*sl = newSl
	}
	return
}

//Delete supprime tous les arguments donnés du slice
func Delete[T comparable](sl *([]T), args ...T) (changed bool) {
	if len(args) > 0 {
		set := SliceToSet(args)
		newSl := make([]T, 0, len(*sl))
		for _, e := range *sl {
			if !set.Contains(e) {
				newSl = append(newSl, e)
			} else {
				changed = true
			}
		}
		*sl = newSl
	}
	return
}

func prepareAdd[T comparable](sl *([]T), args []T) (add []T) {
	set := SliceToSet(*sl)
	for _, e := range args {
		if !set.Contains(e) {
			set.Add(e)
			add = append(add, e)
		}
	}
	return
}

//AddUniq agit comme Add mais ne rajoute que des entrées non dupliquées (ie. non présentes dans le slice)
func AddUniq[T comparable](sl *([]T), args ...T) (changed bool) {
	return Add(sl, prepareAdd(sl, args)...)
}

//InsertUniq agit comme AddUniq mais ajoute les entrées au début
func InsertUniq[T comparable](sl *([]T), args ...T) (changed bool) {
	return Insert(sl, prepareAdd(sl, args)...)
}
