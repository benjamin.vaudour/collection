package collection

import (
	"encoding/json"
	"io"
	"sort"
	"strconv"
	"strings"

	convert "framagit.org/benjamin.vaudour/converter/v2"
)

//Map est un map de strings générique offrant des fonctions supplémentaires utiles
type Map map[string]any

//Exists vérifie que la clé donnée en paramètre est contenue dans la map
func (m Map) Exists(k string) bool {
	_, ok := m[k]
	return ok
}

//Get retourne la valeur de la clé donnée en paramètre
//Retourne nil si la clé n’existe pas
func (m Map) Get(k string) any {
	return m[k]
}

func _sdot(key string) (k0, k1 string, has_next bool, no_error bool) {
	no_error = true
	i := strings.Index(key, ".")
	if has_next = i >= 0; has_next {
		k0, k1 = key[:i], key[i+1:]
	} else {
		k0 = key
	}
	return
}

func _sbracket(key string) (k0, k1 string, has_next bool, no_error bool) {
	i := strings.Index(key, "[")
	if has_next = i >= 0; has_next {
		k0, k1 = key[:i], key[i+1:]
		if no_error = !strings.Contains(k0, "]"); no_error {
			i = strings.Index(k1, "]")
			if no_error = i >= 0 && (len(k1) == i+1 || k1[i+1] == '['); no_error {
				k1 = k1[:i] + k1[i+1:]
			}
		}
	} else {
		k0, no_error = key, !strings.Contains(key, "]")
	}
	return
}

func _grec(e any, k string, spl func(string) (string, string, bool, bool)) (v any, ok bool) {
	switch {
	case convert.IsSlice(e):
		var sl []any
		convert.Convert(e, &sl, true)
		k0, k1, has_next, no_error := spl(k)
		if no_error {
			if idx, err := strconv.Atoi(k0); err == nil && idx >= 0 && idx < len(sl) {
				v, ok = sl[idx], true
				if has_next {
					v, ok = _grec(v, k1, spl)
				}
			}
		}
	case convert.IsMap(e):
		var m Map
		if !convert.Convert(e, &m, true) {
			return
		}
		if v, ok = m[k]; !ok {
			k0, k1, has_next, no_error := spl(k)
			if no_error && has_next {
				if v, ok = m[k0]; ok {
					v, ok = _grec(v, k1, spl)
				}
			}
		}
	}
	return
}

func (m Map) _grec(k string, spl func(string) (string, string, bool, bool)) (any, bool) {
	if v, ok := m[k]; ok {
		return v, ok
	}
	k0, k1, has_next, no_error := spl(k)
	if has_next && no_error {
		if v, ok := m[k0]; ok {
			return _grec(v, k1, spl)
		}
	}
	return nil, false
}

//GetRecursive recherche la valeur de la clé donnée de façon récursive
func (m Map) GetRecursive(keys ...string) (v any, ok bool) {
	v, ok = m, true
l:
	for _, k := range keys {
		switch {
		case convert.IsSlice(v):
			var sl []any
			convert.Convert(v, &sl, true)
			idx, err := strconv.Atoi(k)
			if ok = err == nil && idx >= 0 && idx < len(sl); ok {
				v = sl[idx]
			}
		case convert.IsMap(v):
			var m Map
			if convert.Convert(v, &m, true) {
				if v, ok = m[k]; !ok {
					v = nil
					break l
				}
			}
		}
	}
	return
}

//GetRecursiveDot recherche la valeur de la clé donnée de façon récursive
//La clé peut être de la forme a.b.c... :
// - Si la clé existe, retourne la valeur
// - Si la valeur de la clé 'a' est une map, réitère la recherche de la valeur sur b.c... sur cette valeur
// - Sinon, retourne nil
func (m Map) GetRecursiveDot(k string) (any, bool) {
	return m._grec(k, _sdot)
}

//GetRecursiveBracket recherche la valeur de la clé donnée de façon récursive
//La clé peut être de la forme a[b][c]... :
// - Si la clé existe, retourne la valeur
// - Si la valeur de la clé 'a' est une map, réitère la recherche de la valeur sur b[c]... sur cette valeur
// - Sinon, retourne nil
func (m Map) GetRecursiveBracket(k string) (any, bool) {
	return m._grec(k, _sbracket)
}

//ParseIfExact recherche une valeur de manière récursive
//et si elle la trouve et que e est un pointeur vers une variable
//équivalente à la variable trouvée, la valeur du pointeur est modifiée.
func (m Map) ParseIfExact(e any, keys ...string) bool {
	if v, ok := m.GetRecursive(keys...); ok {
		return convert.Convert(v, e, true)
	}
	return false
}

//Parse agit comme ParseIfExact, à ceci près
//que si la valeur est trouvée, la conversion est forcée
//pour pouvoir modifier la valeur du pointeur.
func (m Map) Parse(e any, keys ...string) bool {
	if v, ok := m.GetRecursive(keys...); ok {
		return convert.Convert(v, e)
	}
	return false
}

//Set associe la clé k avec la valeur v dans la map
func (m Map) Set(k string, v any) Map {
	m[k] = v
	return m
}

//SetIf agit comme Set mais de façon conditionnelle
func (m Map) SetIf(k string, v any, cb func() bool) Map {
	if cb() {
		m.Set(k, v)
	}
	return m
}

//SetIfNotExists agit comme Set que si la clé n’est pas définie dans la map
func (m Map) SetIfNotExists(k string, v any) Map {
	return m.SetIf(k, v, func() bool { return !m.Exists(k) })
}

//Delete supprime toutes les clés données en entrée
func (m Map) Delete(keys ...string) Map {
	for _, k := range keys {
		delete(m, k)
	}
	return m
}

//Clone effectue une copie profonde de la map
func (m Map) Clone(deep ...bool) Map {
	c := make(Map)
	for k, v := range m {
		c.Set(k, convert.CloneInterface(v, deep...))
	}
	return c
}

func (m Map) Is(k string, cb func(any) bool) bool {
	v, exist := m[k]
	return exist && cb(v)
}

//IsInt vérifie que la clé existe que sa valeur associée est de type Int
func (m Map) IsInt(k string) bool { return m.Is(k, convert.IsInt) }

//IsInt vérifie que la clé existe que sa valeur associée est de type Uint
func (m Map) IsUint(k string) bool { return m.Is(k, convert.IsUint) }

//IsFloat vérifie que la clé existe que sa valeur associée est de type Float
func (m Map) IsFloat(k string) bool { return m.Is(k, convert.IsFloat) }

//IsComplex vérifie que la clé existe que sa valeur associée est de type Complex
func (m Map) IsComplex(k string) bool { return m.Is(k, convert.IsComplex) }

//IsChar vérifie que la clé existe que sa valeur associée est de type Char
func (m Map) IsChar(k string) bool { return m.Is(k, convert.IsChar) }

//IsString vérifie que la clé existe que sa valeur associée est de type String
func (m Map) IsString(k string) bool { return m.Is(k, convert.IsString) }

//IsBool vérifie que la clé existe que sa valeur associée est de type Bool
func (m Map) IsBool(k string) bool { return m.Is(k, convert.IsBool) }

//IsSlice vérifie que la clé existe que sa valeur associée est de type slice
func (m Map) IsSlice(k string) bool { return m.Is(k, convert.IsSlice) }

//IsMap vérifie que la clé existe que sa valeur associée est de type map
func (m Map) IsMap(k string) bool { return m.Is(k, convert.IsMap) }

//IsSet vérifie que la clé existe que sa valeur associée est de type Set
func (m Map) IsSet(k string) bool { return m.Is(k, convert.IsSet) }

//GetFormat retourne la valeur associée à la clé si celle-ci existe est est du type demandé
/*func (m Map) GetFormat[T any](k string) (out T, ok bool) {
	if v, exist := m[k]; exist {
		ok = convert.Convert(v, &out, true)
	}
	return
}*/

//ToFormat retourne la valeur associée la clé convertie au format demandé.
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
/*func (m Map) ToFormat[T any](k string, def ...T) (out T) {
	setDef := true
	if v, exist := m[k]; exist {
		setDef = !convert.Convert(v, &out)
	}
	if setDef && len(def) > 0 {
		out = def[0]
	}
	return
}*/

func (m Map) _get(k string, e interface{}) (ok bool) {
	if v, exist := m[k]; exist {
		ok = convert.Convert(v, e, true)
	}
	return
}

//GetInt retourne la valeur associée à la clé si celle-ci existe et est de type Int
func (m Map) GetInt(k string) (out int, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetUint retourne la valeur associée à la clé si celle-ci existe et est de type Uint
func (m Map) GetUint(k string) (out uint, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetFloat retourne la valeur associée à la clé si celle-ci existe et est de type Float
func (m Map) GetFloat(k string) (out float64, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetString retourne la valeur associée à la clé si celle-ci existe et est de type String
func (m Map) GetString(k string) (out string, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetBool retourne la valeur associée à la clé si celle-ci existe et est de type Bool
func (m Map) GetBool(k string) (out bool, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetSlice retourne la valeur associée à la clé si celle-ci existe et est de type slice
func (m Map) GetSlice(k string) (out []interface{}, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetIntSlice retourne la valeur associée à la clé si celle-ci existe et est de type slice d’Int
func (m Map) GetIntSlice(k string) (out []int, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetUintSlice retourne la valeur associée à la clé si celle-ci existe et est de type slice d’Uint
func (m Map) GetUintSlice(k string) (out []uint, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetFloatSlice retourne la valeur associée à la clé si celle-ci existe et est de type slice de Float
func (m Map) GetFloatSlice(k string) (out []float64, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetStringSlice retourne la valeur associée à la clé si celle-ci existe et est de type slice de String
func (m Map) GetStringSlice(k string) (out []string, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetBoolSlice retourne la valeur associée à la clé si celle-ci existe et est de type slice de Bool
func (m Map) GetBoolSlice(k string) (out []bool, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetMap retourne la valeur associée à la clé si celle-ci existe et est de type map
func (m Map) GetMap(k string) (out Map, ok bool) {
	out = make(Map)
	ok = m._get(k, &out)
	return
}

//GetIntMap retourne la valeur associée à la clé si celle-ci existe et est de type map d’Int
func (m Map) GetIntMap(k string) (out map[string]int, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetUintMap retourne la valeur associée à la clé si celle-ci existe et est de type map d’Uint
func (m Map) GetUintMap(k string) (out map[string]uint, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetFloatMap retourne la valeur associée à la clé si celle-ci existe et est de type map de Float
func (m Map) GetFloatMap(k string) (out map[string]float64, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetStringMap retourne la valeur associée à la clé si celle-ci existe et est de type map de String
func (m Map) GetStringMap(k string) (out map[string]string, ok bool) {
	ok = m._get(k, &out)
	return
}

//GetBoolMap retourne la valeur associée à la clé si celle-ci existe et est de type map de Bool
func (m Map) GetBoolMap(k string) (out map[string]bool, ok bool) {
	ok = m._get(k, &out)
	return
}

func (m Map) _2(k string, e interface{}) (ok bool) {
	if v, exist := m[k]; exist {
		ok = convert.Convert(v, e)
	}
	return
}

//ToInt retourne la valeur associée la clé convertie en Int
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToInt(k string, def ...int) (out int) {
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToUint retourne la valeur associée la clé convertie en Uint
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToUint(k string, def ...uint) (out uint) {
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToFloat retourne la valeur associée la clé convertie en Float
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToFloat(k string, def ...float64) (out float64) {
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToString retourne la valeur associée la clé convertie en String
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToString(k string, def ...string) (out string) {
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToBool retourne la valeur associée la clé convertie en Bool
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToBool(k string, def ...bool) (out bool) {
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToSlice retourne la valeur associée la clé convertie en slice
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToSlice(k string, def ...interface{}) (out []interface{}) {
	if !m._2(k, &out) {
		out = def
	}
	return
}

//ToIntSlice retourne la valeur associée la clé convertie en slice d’Int
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToIntSlice(k string, def ...int) (out []int) {
	if !m._2(k, &out) {
		out = def
	}
	return
}

//ToUintSlice retourne la valeur associée la clé convertie en slice d’Uint
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToUintSlice(k string, def ...uint) (out []uint) {
	if !m._2(k, &out) {
		out = def
	}
	return
}

//ToFloatSlice retourne la valeur associée la clé convertie en slice de Float
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToFloatSlice(k string, def ...float64) (out []float64) {
	if !m._2(k, &out) {
		out = def
	}
	return
}

//ToStringSlice retourne la valeur associée la clé convertie en slice de string
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToStringSlice(k string, def ...string) (out []string) {
	if !m._2(k, &out) {
		out = def
	}
	return
}

//ToBoolSlice retourne la valeur associée la clé convertie en slice de Bool
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToBoolSlice(k string, def ...bool) (out []bool) {
	if !m._2(k, &out) {
		out = def
	}
	return
}

//ToMap retourne la valeur associée la clé convertie en map
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToMap(k string, def ...Map) (out Map) {
	out = make(Map)
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToIntMap retourne la valeur associée la clé convertie en map d’Int
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToIntMap(k string, def ...map[string]int) (out map[string]int) {
	out = make(map[string]int)
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToUintMap retourne la valeur associée la clé convertie en map d’Uint
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToUintMap(k string, def ...map[string]uint) (out map[string]uint) {
	out = make(map[string]uint)
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToFloatMap retourne la valeur associée la clé convertie en map de Float
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToFloatMap(k string, def ...map[string]float64) (out map[string]float64) {
	out = make(map[string]float64)
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToStringMap retourne la valeur associée la clé convertie en map de String
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToStringMap(k string, def ...map[string]string) (out map[string]string) {
	out = make(map[string]string)
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//ToBoolMap retourne la valeur associée la clé convertie en map de Bool
//Si la clé n’existe pas ou que la valeur n’est pas convertible, retourne l’éventuelle valeur donnée par défaut
func (m Map) ToBoolMap(k string, def ...map[string]bool) (out map[string]bool) {
	out = make(map[string]bool)
	if !m._2(k, &out) && len(def) > 0 {
		out = def[0]
	}
	return
}

//Fusion ajoute toutes les entrées de la map m2 dans la map m1 et retourne m1
func (m1 Map) Fusion(m2 Map) Map {
	for k, v := range m2 {
		m1[k] = v
	}
	return m1
}

func (m Map) _srec(k string, v any, spl func(string) (string, string, bool, bool)) Map {
	k0, k1, has_next, no_error := spl(k)
	if no_error {
		if has_next {
			rec := make(Map)
			if m.IsMap(k0) {
				rec = m.ToMap(k0)
			}
			rec = rec._srec(k1, v, spl)
			m.Set(k0, rec).Delete(k)
		} else {
			m.Set(k0, v)
		}
	}
	return m
}

//SetRecursive agit comme Set mais de façon récursive
func (m Map) SetRecursive(v any, keys ...string) Map {
	if len(keys) > 0 {
		k := keys[0]
		keys = keys[1:]
		if len(keys) == 0 {
			m.Set(k, v)
		} else {
			rec := make(Map)
			if m.IsMap(k) {
				rec = m.ToMap(k)
			}
			rec = rec.SetRecursive(v, keys...)
			m.Set(k, rec)
		}
	}
	return m
}

//SetRecursiveDot agit comme Set mais de façon récursive
//Une clé récursive est de la forme a.b.c...
func (m Map) SetRecursiveDot(k string, v any) Map {
	return m._srec(k, v, _sdot)
}

//SetRecursiveBracket agit comme Set mais de façon récursive
//Une clé récursive est de la forme a[b][c]...
func (m Map) SetRecursiveBracket(k string, v any) Map {
	return m._srec(k, v, _sbracket)
}

func (m Map) _2sl() any {
	var sidx []string
	var idx []int
	midx := make(map[string]int)
	is_string := false
	for k := range m {
		if m.IsMap(k) {
			m.Set(k, m.ToMap(k)._2sl())
		} else if m.IsSlice(k) {
			sl := m.ToSlice(k)
			for i, v := range sl {
				var mv Map
				if convert.Convert(v, &mv, true) {
					sl[i] = mv._2sl()
				}
			}
			m.Set(k, sl)
		}
		if i, err := strconv.Atoi(k); err != nil {
			is_string = true
		} else {
			sidx = append(sidx, k)
			idx = append(idx, i)
			midx[k] = i
		}
	}
	if is_string {
		return m
	}
	sort.Ints(idx)
	for i, e := range idx {
		if i != e {
			return m
		}
	}
	sort.Slice(sidx, func(i, j int) bool { return midx[sidx[i]] < midx[sidx[j]] })
	out := make([]any, len(sidx))
	for i, k := range sidx {
		out[i] = m.Get(k)
	}
	return out
}

//FormatDot convertit les clés récursives, si possible.
//Exemple: Soit une map ayant la représentation json suivante:
//  {
//    "a.0": "v1",
//    "a.1": "v2",
//    "b.1.c": "v3",
//    "b.1.d": "v4",
//    "b.2": "v5",
//    "b": {
//      "3.0": "v6",
//    }
//  }
//
//Après formatage, la représentation sera la suivante:
//  {
//    "a": [
//      "v1",
//      "v2"
//    ],
//    "b": {
//      "1": {
//        "c": "v3",
//        "d": "v4"
//      },
//      "2": "v5",
//      "3": [
//        "v6"
//      ]
//    }
//  }
func (m Map) FormatDot() Map {
	for k, v := range m {
		m.SetRecursiveDot(k, v)
	}
	for k := range m {
		if v, ok := m.GetMap(k); ok {
			m.Set(k, v._2sl())
		}
	}
	return m
}

//FormatBracket convertit les clés récursives, si possible.
//Exemple: Soit une map ayant la représentation json suivante:
//  {
//    "a[0]": "v1",
//    "a[1]": "v2",
//    "b[1][c]": "v3",
//    "b[1][d]": "v4",
//    "b[2]": "v5",
//    "b": {
//      "3[0]": "v6",
//    }
//  }
//
//Après formatage, la représentation sera la suivante:
//  {
//    "a": [
//      "v1",
//      "v2"
//    ],
//    "b": {
//      "1": {
//        "c": "v3",
//        "d": "v4"
//      },
//      "2": "v5",
//      "3": [
//        "v6"
//      ]
//    }
//  }
func (m Map) FormatBracket() Map {
	for k, v := range m {
		m.SetRecursiveBracket(k, v)
	}
	for k := range m {
		if v, ok := m.GetMap(k); ok {
			m.Set(k, v._2sl())
		}
	}
	return m
}

//SliceMap est un tableau de maps
type SliceMap []Map

func (sl *SliceMap) Add(maps ...Map) SliceMap {
	*sl = append(*sl, maps...)
	return *sl
}

func (sl *SliceMap) Insert(maps ...Map) SliceMap {
	*sl = append(maps, (*sl)...)
	return *sl
}

type MapDecoder struct {
	d *json.Decoder
}

func NewMapDecoder(r io.Reader) *MapDecoder { return &MapDecoder{d: json.NewDecoder(r)} }

func (dec *MapDecoder) Decode() (m Map, err error) {
	m = make(Map)
	err = dec.d.Decode(&m)
	return
}

func (dec *MapDecoder) DecodeSlice() (sl SliceMap, err error) {
	err = dec.d.Decode(&sl)
	return
}

func (m Map) Decode(r io.Reader) (Map, error) {
	dec := NewMapDecoder(r)
	n, err := dec.Decode()
	if err == nil {
		m = m.Fusion(n)
	}
	return m, err
}

func (sl *SliceMap) Decode(r io.Reader) (SliceMap, error) {
	dec := NewMapDecoder(r)
	n, err := dec.DecodeSlice()
	if err == nil {
		return sl.Add(n...), err
	}
	return *sl, err
}

type MapEncoder struct {
	e *json.Encoder
}

func NewMapEncoder(w io.Writer) *MapEncoder { return &MapEncoder{e: json.NewEncoder(w)} }

func (enc *MapEncoder) Encode(m Map) error { return enc.e.Encode(m) }

func (enc *MapEncoder) EncodeSlice(sl SliceMap) error { return enc.e.Encode(sl) }

func (enc *MapEncoder) SetEscapeHTML(on bool) { enc.e.SetEscapeHTML(on) }

func (enc *MapEncoder) SetIndent(prefix, indent string) { enc.e.SetIndent(prefix, indent) }

func (m Map) Encode(w io.Writer) error {
	enc := NewMapEncoder(w)
	return enc.Encode(m)
}

func (m Map) EncodeHuman(w io.Writer) error {
	enc := NewMapEncoder(w)
	enc.SetIndent("", "  ")
	return enc.Encode(m)
}

func (sl SliceMap) Encode(w io.Writer) error {
	enc := NewMapEncoder(w)
	return enc.EncodeSlice(sl)
}

func (sl SliceMap) EncodeHuman(w io.Writer) error {
	enc := NewMapEncoder(w)
	enc.SetIndent("", "  ")
	return enc.EncodeSlice(sl)
}
