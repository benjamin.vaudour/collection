package collection

var empty struct{}

//Set est une collection de variables uniques
type Set[T comparable] map[T]struct{}

//Add ajoute au set les variables données en paramètre
func (s Set[T]) Add(args ...T) Set[T] {
	for _, e := range args {
		s[e] = empty
	}
	return s
}

//Del supprime du set les variables données en paramètre
func (s Set[T]) Del(args ...T) Set[T] {
	for _, e := range args {
		delete(s, e)
	}
	return s
}

//Contains vérifie que le set contient au moins une des variables données en paramètre
func (s Set[T]) Contains(args ...T) bool {
	for _, e := range args {
		if _, ok := s[e]; ok {
			return true
		}
	}
	return false
}

//ContainsAll vérifie que le set contient toutes les variables données en paramètre
func (s Set[T]) ContainsAll(args ...T) bool {
	for _, e := range args {
		if _, ok := s[e]; !ok {
			return false
		}
	}
	return true
}

//Slice retourne un slice avec toutes les entrées du set
func (s Set[T]) Slice() []T {
	var out []T
	for e := range s {
		out = append(out, e)
	}
	return out
}

//Len retourne le nombre de variables contenues dans le set
func (s Set[T]) Len() int {
	return len(s)
}

//NewSet retourne un set initialisé par les variables données en entrée
func NewSet[T comparable](args ...T) Set[T] {
	set := make(Set[T])
	return set.Add(args...)
}
