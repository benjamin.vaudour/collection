package collection

import (
	"reflect"

	convert "framagit.org/benjamin.vaudour/converter"
)

//Slice convertit la variable donnée en paramètre en slice générique
func Slice(sl interface{}) (out []interface{}) {
	sl = convert.ValueOf(sl)
	if convert.IsSlice(sl) {
		convert.Convert(sl, &out)
	}
	return
}

//SliceProperties retourne les propriétés du slice donné en entrée
//Si la variable d’entrée n’est pas un slice ou un pointeur de slice, retourne ok à false
func SliceProperties(e interface{}) (v reflect.Value, t reflect.Type, ok bool) {
	if ok = convert.IsSlice(convert.ValueOf(e)); !ok {
		return
	}
	c := convert.NewChecker(e)
	v, t = c.Value(), c.Type()
	if c.IsPointer() {
		v, t = v.Elem(), t.Elem()
	}
	return
}

//SliceOf convertit un slice générique en empaqueteur de slice de type déterminé par le type t.
func SliceOf(e []interface{}, t reflect.Type) (v reflect.Value, ok bool) {
	p := reflect.New(t)
	if ok = convert.Convert(e, p.Interface()); ok {
		v = p.Elem()
	}
	return
}

//SlicetoSet convertit un slice en un set.
func SliceToSet(e interface{}) (s Set, ok bool) {
	if _, _, ok = SliceProperties(e); ok {
		s = NewSet(Slice(e)...)
	}
	return
}

//Contains vérifie que le slice donné en entrée contient au moins un des arguments spécifiés
func Contains(sl interface{}, args ...interface{}) bool {
	s, ok := SliceToSet(sl)
	return ok && s.Contains(args...)
}

//ContainsAll vérifie que le slice donné en entrée contient tous les arguments spécifiés
func ContainsAll(sl interface{}, args ...interface{}) bool {
	s, ok := SliceToSet(sl)
	return ok && s.ContainsAll(args...)
}

//Add ajoute tous les arguments donnés au slice donné en entrée.
//Nota: sl doit être un pointeur de slice
func Add(sl interface{}, args ...interface{}) (changed bool) {
	if len(args) == 0 || !convert.IsPointer(sl) {
		return
	}
	v0, t, ok := SliceProperties(sl)
	if !ok {
		return
	}
	v1, ok := SliceOf(args, t)
	if !ok {
		return
	}
	v0.Set(reflect.AppendSlice(v0, v1))
	return true
}

//Insert fonctionne comme Add à ceci près que les arguments sont ajoutés au début du slice
func Insert(sl interface{}, args ...interface{}) (changed bool) {
	if len(args) == 0 || !convert.IsPointer(sl) {
		return
	}
	v0, t, ok := SliceProperties(sl)
	if !ok {
		return
	}
	v1, ok := SliceOf(args, t)
	if !ok {
		return
	}
	v0.Set(reflect.AppendSlice(v1, v0))
	return true
}

//InsertAt fonctionne comme Insert à ceci près que les arguments sont ajoutés à la position donnée
//Si la position est ≤ 0, agit comme Insert
//Si la position est supérieur à la taille du slice, agit comme Add
func InsertAt(sl interface{}, pos int, args ...interface{}) (changed bool) {
	if pos <= 0 {
		return Insert(sl, args...)
	}
	if len(args) == 0 || !convert.IsPointer(sl) {
		return
	}
	v0, t, ok := SliceProperties(sl)
	if !ok {
		return
	}
	l0, l1 := v0.Len(), len(args)
	if pos >= l0 {
		return Add(sl, args...)
	}
	v1, ok := SliceOf(args, t)
	if !ok {
		return
	}
	result := reflect.MakeSlice(t, l0+l1, l0+l1)
	for i := 0; i < l0+l1; i++ {
		var v reflect.Value
		if i < pos {
			v = v0.Index(i)
		} else if i < pos+l1 {
			v = v1.Index(i - pos)
		} else {
			v = v0.Index(i - l1)
		}
		result.Index(i).Set(v)
	}
	v0.Set(result)
	return true
}

//Delete supprime tous les arguments donnés du slice
//Nota: sl doit être un pointeur de slice
func Delete(sl interface{}, args ...interface{}) (changed bool) {
	if len(args) == 0 || !convert.IsPointer(sl) {
		return
	}
	v0, t, ok := SliceProperties(sl)
	if !ok {
		return
	}
	s, _ := SliceToSet(args)
	result := reflect.MakeSlice(t, 0, v0.Len())
	for i := 0; i < v0.Len(); i++ {
		v := v0.Index(i)
		if !s[v.Interface()] {
			result = reflect.Append(result, v)
		} else {
			changed = true
		}
	}
	v0.Set(result)
	return
}

//AddUniq agit comme Add mais ne rajoute que des entrées non dupliquées (ie. non présentes dans le slice)
func AddUniq(sl interface{}, args ...interface{}) (changed bool) {
	if len(args) == 0 || !convert.IsPointer(sl) {
		return false
	}
	v0, t, ok := SliceProperties(sl)
	if !ok {
		return
	}
	s, _ := SliceToSet(sl)
	result := reflect.MakeSlice(t, 0, v0.Cap()+len(args))
	result = reflect.AppendSlice(result, v0)
	for _, e := range args {
		if !s[e] {
			result = reflect.Append(result, reflect.ValueOf(e))
			s.Add(e)
			changed = true
		}
	}
	v0.Set(result)
	return
}

//InsertUniq agit comme AddUniq mais ajoute les entrées au début
func InsertUniq(sl interface{}, args ...interface{}) (changed bool) {
	if len(args) == 0 || !convert.IsPointer(sl) {
		return false
	}
	v0, t, ok := SliceProperties(sl)
	if !ok {
		return
	}
	s, _ := SliceToSet(sl)
	result, _ := SliceOf(args, t)
	for _, e := range args {
		if !s[e] {
			result = reflect.Append(result, reflect.ValueOf(e))
			s.Add(e)
			changed = true
		}
	}
	v0.Set(reflect.Append(result, v0))
	return
}
